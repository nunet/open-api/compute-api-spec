# Compute API Specification

This repository shows the API Spec for the Compute API.

The generated documentation links can be found here:

## Swagger
- Stable (Main Branch) http://openapi.nunet.io/compute-api-spec/main/swagger/
- Beta (Staging Branch) http://openapi.nunet.io/compute-api-spec/staging/swagger/
- Alpha (Develop Branch) http://openapi.nunet.io/compute-api-spec/develop/swagger/

## Protobuf: how to use the .proto file in your repository
1. Clone this repository as a git submodule in your project repository
2. Compile the .proto file in the language of choice
